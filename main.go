package main

import (
	"fmt"
	"net"
	"time"

	"github.com/BurntSushi/toml"
)

type Config struct {
	Server   string
	Rate     int
	Workers  int
	Duration string
	Format   string
	Template string
}

var config Config

func main() {
	// load config file
	if _, err := toml.DecodeFile("config.toml", &config); err != nil {
		fmt.Println(err)
		return
	}

	st := time.Now()

	serverAddr, err := net.ResolveUDPAddr("udp", config.Server)
	if err != nil {
		fmt.Println("!!! Check server address Error...")
		return
	}
	conn, err := net.DialUDP("udp", nil, serverAddr)
	defer conn.Close()

	dt, err := time.ParseDuration(config.Duration)
	if err != nil {
		fmt.Println("... Check config duration Error. Use 10s instand...")
		dt, _ := time.ParseDuration("10s")
	}
	for d := 0; d < dt.Seconds(); d++ {
		t0 := time.Now()

		for i := 0; config.Rate > i; i++ {
			str := time.Now().Format("Jan 2 15:04:05") + config.Template
			conn.Write([]byte(str))
		}
		time.Sleep(time.Second - time.Now().Sub(t0))

	}
	sp := time.Now().Sub(st)
	fmt.Println("====  send 10000 packets used", sp)
}
